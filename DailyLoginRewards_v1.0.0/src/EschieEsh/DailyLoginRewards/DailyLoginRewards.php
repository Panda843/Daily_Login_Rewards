<?php

namespace EschieEsh\DailyLoginRewards;

use pocketmine\Player;
use pocketmine\item\Item;
use pocketmine\utils\Config;
use pocketmine\plugin\PluginBase;
use pocketmine\event\Listener;
use pocketmine\utils\TextFormat;
use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\command\CommandExecutor;
use SimpleAuth\event\PlayerAuthenticateEvent;

class DailyLoginRewards extends PluginBase implements Listener {
   
    public function get_command_usage() : array {
        return array(
            "--D-A-I-L-Y---L-O-G-I-N---R-E-W-A-R-D-S--",
            "/dlr get -> [Get your next daily reward]",
            "/dlr info -> [List of all daily rewards]",
            "-----------------------------------------"
        );
    }
    public function print_command_usage($sender) {
        for($i = 0; $i < count($this->get_command_usage()); $i++){
            $sender->sendMessage($this->format_message(TextFormat::AQUA.$this->get_command_usage()[$i]));
        }
    }
    public function format_message($message) : string {
        return TextFormat::ITALIC.TextFormat::BLUE."{".TextFormat::GREEN."DailyRewards".TextFormat::BLUE."} ".$message;
    }
    public function format_time($seconds) : array {
        $seconds_in_min = 60;
        $seconds_in_hour = 60 * $seconds_in_min;
        $seconds_in_day = 24 * $seconds_in_hour;
        $hours = floor(($seconds % $seconds_in_day) / $seconds_in_hour);
        $minutes = floor((($seconds % $seconds_in_day) % $seconds_in_hour) / $seconds_in_min);
        $seconds = ceil((($seconds % $seconds_in_day) % $seconds_in_hour) % $seconds_in_min);
        return array(
            'h' => (int) $hours,
            'm' => (int) $minutes,
            's' => (int) $seconds,
        );
    }
    public function onEnable(){
        @mkdir($this->getDataFolder());
        @mkdir($this->getDataFolder() . "data/");
        $this->getLogger()->info(TextFormat::GREEN.TextFormat::ITALIC."DailyLoginRewards has been enabled!");
        $this->saveDefaultConfig();
        $this->config = new Config($this->getDataFolder()."config.yml", Config::YAML, array( 
            'daily_items' => array('day' => array(
                '1' => array('260=item_id/item_count=8', '17=item_id/item_count=20', '4=item_id/item_count=64', '384=item_id/item_count=1'),
                '2' => array('17=item_id/item_count=50', '260=item_id/item_count=16', '4=item_id/item_count=128', '263=item_id/item_count=15', '384=item_id/item_count=5'),
                '3' => array('260=item_id/item_count=32', '265=item_id/item_count=8', '266=item_id/item_count=12', '384=item_id/item_count=10'),
                '4' => array('260=item_id/item_count=64', '265=item_id/item_count=16', '266=item_id/item_count=20', '264=item_id/item_count=3', '384=item_id/item_count=20'),
                '5' => array('49=item_id/item_count=15', '264=item_id/item_count=10', '384=item_id/item_count=64'),
                '6' => array('266=item_id/item_count=32', '265=item_id/item_count=32', '264=item_id/item_count=15', '384=item_id/item_count=64'),
                '7' => array('388=item_id/item_count=1', '264=item_id/item_count=25', '384=item_id/item_count=64', '384=item_id/item_count=64'),
                )
            )
        ));
        $this->getServer()->getPluginManager()->registerEvents($this, $this);
    }
    
    public function onJoin(PlayerAuthenticateEvent $event){ // SimpleAuth support
        $this->getServer()->getScheduler()->scheduleDelayedTask(new RewardsTask($this,$event->getPlayer()),10);
    }
    public function onCommand(CommandSender $sender, Command $command, $label, array $args){
        if($command->getName() == "dlr"){
            if($sender instanceof Player){
                if(!isset($args[0])){
                    $this->print_command_usage($sender);
                } else {
                    if($args[0]=="get"){
                        $this->getServer()->getScheduler()->scheduleDelayedTask(new RewardsTask($this,$sender),10);
                    } else if($args[0]=="info"){
                        $this->getServer()->getScheduler()->scheduleDelayedTask(new InfoTask($this,$sender),10);
                    } else {
                        $this->print_command_usage($sender);
                    }
                }
            } else {
                $sender->sendMessage($this->format_message(TextFormat::RED."You must run that command in the game!"));
            }
        } 
    }
    public function onDisable(){
        $this->getLogger()->info(TextFormat::RED.TextFormat::ITALIC."DailyLoginRewards has been disabled!");
    }
    
}