<?php 

namespace EschieEsh\DailyLoginRewards;

use pocketmine\Player;
use pocketmine\item\Item;
use pocketmine\utils\Config;
use pocketmine\utils\TextFormat;
use pocketmine\inventory\Inventory;
use pocketmine\scheduler\PluginTask;

class InfoTask extends PluginTask{
    public function __construct(DailyLoginRewards $plugin, Player $player){ 
        $this->plugin = $plugin;
        $this->player = $player;
        parent::__construct($plugin);
    }
    public function onRun($tick){ 
        $player = $this->player->getName();
        $daily_items = $this->plugin->config->getAll();
        $player_reward = (new Config($this->plugin->getDataFolder() . "data/" . strtolower($player[0]) . "/" . "$player.yml", Config::YAML))->getAll();
        for($day = 1; isset($daily_items['daily_items']['day'][$day]); $day++){
            $items="";
            for($item=0;$item<count($daily_items['daily_items']['day'][$day]);$item=$item+1){
                $data_of_item = explode("=item_id/item_count=",$daily_items['daily_items']['day'][$day][$item]);
                $item_data = new Item($data_of_item[0],0,$data_of_item[1]);
                $item_name = $item_data->get($data_of_item[0])->getName();
                $items = "$items $data_of_item[1] $item_name | ";
            }
            isset($player_reward["day"]) ? $day_of_the_reward = $player_reward["day"] : $day_of_the_reward = 1;
            if($day_of_the_reward > $day){
                $this->player->sendMessage($this->plugin->format_message(TextFormat::DARK_AQUA."Day $day : $items"));
            } else if($day_of_the_reward == $day){
                $this->player->sendMessage($this->plugin->format_message(TextFormat::GOLD."NEXT : Day $day : $items"));
            } else {
                $this->player->sendMessage($this->plugin->format_message(TextFormat::DARK_AQUA."Day $day : $items"));
            }
        }
    }
}