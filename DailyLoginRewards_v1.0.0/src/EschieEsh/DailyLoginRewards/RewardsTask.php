<?php 

namespace EschieEsh\DailyLoginRewards;

use pocketmine\Player;
use pocketmine\item\Item;
use pocketmine\utils\Config;
use pocketmine\utils\TextFormat;
use pocketmine\inventory\Inventory;
use pocketmine\scheduler\PluginTask;

class RewardsTask extends PluginTask{
    public function __construct(DailyLoginRewards $plugin, Player $player){ 
        $this->plugin = $plugin;
        $this->player = $player;
        parent::__construct($plugin, $player);
    }
    public function onRun($tick){ 
        if($this->player->getGamemode()==Player::SURVIVAL or $this->player->getGamemode()==Player::ADVENTURE){
            $name_of_player = $this->player->getName();
            $daily_items = $this->plugin->config->getAll();
            @mkdir($this->plugin->getDataFolder() . "data/" . strtolower($name_of_player[0]) . "/");
            $this->plugin->saveResource($this->plugin->getDataFolder() . "data/" . strtolower($name_of_player[0]) . "/" . "$name_of_player.yml");
            $config = (new Config($this->plugin->getDataFolder() . "data/" . strtolower($name_of_player[0]) . "/" . "$name_of_player.yml", Config::YAML))->getAll();
            isset($config['time']) ? $day = (int) $config['day'] : $day = 1;
            $items = $this->player->getInventory()->getContents();
            if(count($daily_items['daily_items']['day'][$day]) <= 36 - count($items)){
                if(!isset($config["time"]) or $config["time"]<time()){
                    
                    for($item=0;$item<count($daily_items['daily_items']['day'][$day]);$item=$item+1){
                        $data_of_item = explode("=item_id/item_count=",$daily_items['daily_items']['day'][$day][$item]);
                        $item_to_be_given = new Item($data_of_item[0],0,$data_of_item[1]);
                        $this->player->getInventory()->addItem($item_to_be_given);
                    }
                    $this->player->sendMessage($this->plugin->format_message(TextFormat::AQUA . "You have successfully received your daily reward for today! Come back tomorrow for another reward!")); 
                    $config = new Config($this->plugin->getDataFolder() . "data/" . strtolower($name_of_player[0]) . "/" . "$name_of_player.yml", Config::YAML);
                    $config->set("time",(int) time()+24*3600);
                    isset($daily_items['daily_items']['day'][$day+1]) ? $config->set("day",$day+1) : $config->set("day",1);
                    
                    $config->save();
                } else {
                    $time = $this->plugin->format_time($config["time"]-time());
                    $hours = $time['h'];
                    $minutes = $time['m'];
                    $seconds = $time['s'];
                    $hours<10 ? $hours="0$hours" : 1;
                    $minutes<10 ? $minutes="0$minutes" : 1;
                    $seconds<10 ? $seconds="0$seconds" : 1;
                    $real_time = array($hours,$minutes,$seconds);
                    $this->player->sendMessage($this->plugin->format_message(TextFormat::RED . "You have already received your daily reward for today! Come back after ".TextFormat::AQUA."$real_time[0]h $real_time[1]m $real_time[2]s".TextFormat::RED."! -> Use */dlr info* to check your rewards for the following days!"));
                }
            } else {
                $amount = count($daily_items['daily_items']['day'][$day]) - 36 + count($items);
                $this->player->sendMessage($this->plugin->format_message(TextFormat::RED."Your inventory space is not enough! Free $amount more inventory slots to get your daily reward! -> You can use */dlr get* to get it after you clean some slots!"));
            }
        } else {
            $this->player->sendMessage($this->plugin->format_message(TextFormat::RED."You couldn't receive your daily reward because you are in creative/spectator mode! -> Use */dlr get* to get it after you change gamemodes!"));
        }
    }
}